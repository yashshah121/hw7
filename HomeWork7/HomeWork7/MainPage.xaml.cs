﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HomeWork7
{
    
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        //Object creation

        WeatherService _WeatherService;

        public MainPage()
        {
            InitializeComponent();
            _WeatherService = new WeatherService();
            //calling following function
            OnGetWeather();
        }

        async void OnGetWeather()
        {
            try
            {
                WeatherData weatherData = await _WeatherService.GetWeatherData(GenerateRequestUri());

                descriptionTxt.Text = weatherData.Weather[0].Description.ToUpper();
                iconImg.Source = $"w{weatherData.Weather[0].icon}";
                cityTxt.Text = weatherData.Name.ToUpper();
                temperatureTxt.Text = weatherData.Main.Temperature.ToString("0");
                humidityTxt.Text = $"{weatherData.Main.Humidity}%";
                pressureTxt.Text = $"{weatherData.Main.Pressure} hpa";
                windTxt.Text = $"{weatherData.Wind.speed} m/s";
                cloudinessTxt.Text = $"{weatherData.Clouds.all}%";
                /*
               I was not able to right code for upcoming days but i learned something about making better UI.
                
                dayOneTxt.Text = DateTime.Parse(allList[0].dt_txt).ToString("dddd");
                dateOneTxt.Text = DateTime.Parse(allList[0].dt_txt).ToString("dd MMM");
                iconOneImg.Source = $"w{allList[0].weather[0].icon}";
                tempOneTxt.Text = allList[0].main.Temperature.ToString("0");

                dayTwoTxt.Text = DateTime.Parse(allList[1].dt_txt).ToString("dddd");
                dateTwoTxt.Text = DateTime.Parse(allList[1].dt_txt).ToString("dd MMM");
                iconTwoImg.Source = $"w{allList[1].weather[0].icon}";
                tempTwoTxt.Text = allList[1].main.Temperature.ToString("0");

                dayThreeTxt.Text = DateTime.Parse(allList[2].dt_txt).ToString("dddd");
                dateThreeTxt.Text = DateTime.Parse(allList[2].dt_txt).ToString("dd MMM");
                iconThreeImg.Source = $"w{allList[2].weather[0].icon}";
                tempThreeTxt.Text = allList[2].main.Temperature.ToString("0");

                dayFourTxt.Text = DateTime.Parse(allList[3].dt_txt).ToString("dddd");
                dateFourTxt.Text = DateTime.Parse(allList[3].dt_txt).ToString("dd MMM");
                iconFourImg.Source = $"w{allList[3].weather[0].icon}";
                tempFourTxt.Text = allList[3].main.Temperature.ToString("0");*/
            }
            catch (Exception ex)
            {
                await DisplayAlert("Weather Info", ex.Message, "OK");
            }

        }
        string GenerateRequestUri()
        {
            //requesting URI
            string requestUri = URIAPI.OpenWeatherMapEndpoint;
            requestUri += $"?q=San Marcos";
            requestUri += "&units=imperial"; // or units=metric
            requestUri += $"&APPID={URIAPI.OpenWeatherMapAPIKey}";
            return requestUri;
        }






    }
}

